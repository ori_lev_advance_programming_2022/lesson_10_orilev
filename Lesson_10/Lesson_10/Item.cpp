#include "Item.h"

Item::Item(const std::string& name, const std::string& serialNumber, double unitPrice)
{
	_name = name;
	_serialNumber = serialNumber;
	_unitPrice = unitPrice;
	_count = 1;
}

Item::Item(const Item& other)
{
	_name = other.getName();
	_serialNumber = other.getSerial();
	_unitPrice = other.getPrice();
	_count = other.getCount();
}

Item::~Item()
{
}

std::string Item::getName() const
{
	return _name;
}

std::string Item::getSerial() const
{
	return _serialNumber;
}

double Item::getPrice() const
{
	return _unitPrice;
}

int Item::getCount() const
{
	return _count;
}

void Item::setCount(int newCount)
{
	_count = newCount;
}

bool Item::operator<(const Item& other) const
{
	return _serialNumber < other.getSerial();
}

bool Item::operator>(const Item& other) const
{
	return _serialNumber > other.getSerial();
}

bool Item::operator==(const Item& other) const
{
	return _serialNumber == other.getSerial();
}

double Item::totalPrice() const
{
	return _unitPrice * _count;
}
