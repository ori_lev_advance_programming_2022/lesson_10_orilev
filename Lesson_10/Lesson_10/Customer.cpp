#include "Customer.h"

Customer::Customer(const std::string& name)
{
	_name = name;
}

Customer::Customer(const Customer& other)
{
	_name = other.getName();
	_items = other.getItems();
}

Customer::Customer()
{
	_name = "";
}

Customer::~Customer()
{
}

double Customer::totalSum() const
{
	double sum = 0;
	for (auto i = _items.begin(); i != _items.end(); i++)
		sum += (*i).totalPrice();
	
	return sum;
}

std::set<Item> Customer::getItems() const
{
	return _items;
}

std::string Customer::getName() const
{
	return _name;
}

void Customer::addItem(const Item& item)
{	
	// get the iterator of the item
	std::set<Item>::iterator i = _items.find(item);


	// if the item exist
	if (i != _items.end())
	{
		// copied the item
		Item temp = Item((*i));
		// increase the counter by one
		temp.setCount(temp.getCount() + 1);
		// delete the old item
		_items.erase(item);
		// insert the new item to the set
		_items.insert(temp);
	}
		
	else
		_items.insert(item);
}

void Customer::removeItem(Item item)
{
	std::set<Item>::iterator i = _items.find(item);
	if (i == _items.end())
		return;
	
	if ((*i).getCount() == 1)
		_items.erase(item);

	else
	{
		// copied the item
		Item temp = Item((*i));
		// dec the counter by one
		temp.setCount(temp.getCount() - 1);
		// delete the old item
		_items.erase(item);
		// insert the new item to the set
		_items.insert(temp);
	}
}
