#pragma once

#include <string>


class Item {
public:
	Item(const std::string& name, const std::string& serialNumber, double unitPrice);
	Item(const Item& other);
	~Item();

	// getters
	std::string getName() const;
	std::string getSerial() const;
	double getPrice() const;
	int getCount() const;

	// setters
	void setCount(int newCount);

	// operators
	bool operator <(const Item& other) const;
	bool operator >(const Item& other) const;
	bool operator ==(const Item& other) const;

	// functions
	double totalPrice() const;

private:
	std::string _name;
	std::string _serialNumber;
	int _count;
	double _unitPrice;
};