#include"Customer.h"
#include<map>

#define SIGNUP_NEW_USER 1
#define UPDATE_CUSTONER 2
#define PRINT_THE_CUSTOMER_WHO_PAY_THE_MOST 3
#define EXIT 4

#define ITEM_LIST_SIZE 10

#define ADD 1
#define REMOVE 2
#define BACK_TO_MANU 3

// print the menu
void printMenu() {
	std::cout << "Welcome to MagshiMart!					" << std::endl;
	std::cout << "1. to sign as customer and buy items		" << std::endl;
	std::cout << "2. to update existing customer's items	" << std::endl;
	std::cout << "3. to print the customer who pays the most" << std::endl;
	std::cout << "4. to exit								" << std::endl;
}


// print the list items
void printItemList(const Item* items, int size) {
	int i = 0;

	for (i = 0; i < size; i++)
		std::cout << i + 1 << ". " << items[i].getName() << "  price : " << items[i].getPrice() << std::endl;

	std::cout << "What item would you like to buy? Input: " << std::endl;
}


// insert items to the customer
void fillCustomer(Customer* customer, const Item* items, int size)
{
	int choice = 0;

	do
	{
		// get the user choice
		system("cls");
		std::cout << "The items you can buy are : (0 to exit)" << std::endl;
		printItemList(items, size);
		std::cin >> choice;
		getchar();

		// check if needed to exit
		if (choice == 0)
			return;
		// check if choice is out of the range
		if (choice > size || choice < 1)
		{
			std::cerr << "ERROR: Invalid choice." << std::endl;
			return;
		}

		customer->addItem(items[choice - 1]);
	} while (choice != 0);
}


void removeItemsFromCustomer(Customer* customer, const Item* items, int size)
{	
	int choice = 0;
	
	do
	{
		// get user choice
		system("cls");
		std::cout << "The items you can remove are : (0 to exit)" << std::endl;
		printItemList(items, size);
		std::cin >> choice;
		getchar();
		// check if nedded to exit
		if (choice == 0)
			return;
		// check if the user choice is out of the range
		if (choice > size || choice < 1)
		{
			std::cerr << "ERROR: Invalid choice." << std::endl;
			return;
		}
	
		customer->removeItem(items[choice - 1]);
	} while (choice != 0);
}


void updateCustomer(Customer* customer, const Item* items, int size)
{
	int choice = 0, innerChoice = 0;

	do {
		// print the menu
		std::cout << "1. Add items	 " << std::endl;
		std::cout << "2. Remove items" << std::endl;
		std::cout << "3. Back to menu" << std::endl;

		std::cin >> choice;
		getchar();

		switch (choice)
		{
		case ADD:
			fillCustomer(customer, items, size);
			break;

		case REMOVE:
			removeItemsFromCustomer(customer, items, size);
			break;

		default:
			return;
			break;
		}
	} while (choice != BACK_TO_MANU);
}


// find Bill Gates
void findTheRichGuy(const std::map<std::string, Customer>& customers)
{
	double hieght = 0;
	std::string name;

	for (auto c : customers)
	{
		if (c.second.totalSum() > hieght)
		{
			hieght = c.second.totalSum();
			name = c.first;
		}
	}

	std::cout << "The customer who paied the biggest amount of money is: " << name << ", total: " << hieght << std::endl;
}



int main()
{
	int user_option_input = 0;
	std::string name;

	std::map<std::string, Customer> abcCustomers;
	Item itemList[ITEM_LIST_SIZE] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32) };
	

	while (user_option_input != EXIT)
	{
		// print the menu and get the user choice
		printMenu();
		std::cin >> user_option_input;
		getchar(); // clear the buffer

		switch (user_option_input)
		{
		
		case SIGNUP_NEW_USER:
			// get the new user name
			std::cout << "Enter name: ";
			std::getline(std::cin, name);
			// check if the user exist
			if (abcCustomers.find(name) != abcCustomers.end())
				std::cerr << "ERROR: The customer is already exist!" << std::endl;
			else
			{
				// get the user shoping list and insert him into the hash map
				Customer tempCustomer;
				fillCustomer(&tempCustomer, itemList, ITEM_LIST_SIZE);
				abcCustomers.insert({ name, tempCustomer });
			}
			break;

		case UPDATE_CUSTONER:
			// get the user name
			std::cout << "Enter name: ";
			std::getline(std::cin, name);

			// if the user exist
			if (abcCustomers.find(name) != abcCustomers.end())
			{
				// copied it
				Customer customer = Customer(abcCustomers.find(name)->second);
				// update him
				updateCustomer(&customer, itemList, ITEM_LIST_SIZE);
				// inset him into the table
				abcCustomers.erase(name);
				abcCustomers.insert({ name, customer });
			}
			else
				std::cerr << "ERROR: the are no customer." << std::endl;
			
			break;
		case PRINT_THE_CUSTOMER_WHO_PAY_THE_MOST:
			findTheRichGuy(abcCustomers);
			break;

		case EXIT:
			_exit(1);
			break;
		}
	}
	return 0;
}