#pragma once

#include "Item.h"


#include <iostream>
#include <string>
#include <set>


class Customer {
public:
	Customer(const std::string& name);
	Customer(const Customer& other);
	Customer();

	~Customer();

	double totalSum() const;

	std::set<Item> getItems() const;
	std::string getName() const;

	void addItem(const Item& item);

	void removeItem(Item item);

private:
	std::string _name;
	std::set<Item> _items;
};